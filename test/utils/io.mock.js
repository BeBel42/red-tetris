export class IOMock {

    toEmit = jest.fn(() => {});
    to = jest.fn(() => {
       return {
           emit: this.toEmit
       };
    });

}
