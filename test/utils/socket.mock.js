export class SocketMock {
    on = jest.fn(() => {});
    to = jest.fn(() => {
        return {
            emit: this.emit
        };
    });
    emit = jest.fn(() => {});
    join = jest.fn(() => {});
}
