'use strict';

import chai, { expect } from "chai"
chai.should();

import delay from '../../src/client/utilities/delay.js'

// mock server and socket configs
import { setMockServer, stopMockServer, closeClient } from '../helpers/wsHelpers.js';
// setting server responses
setMockServer({
    nextPiece: [
        [
            {id: 4, orientation: 1},
            {id: 3, orientation: 0},
            {id: 2, orientation: 2}
        ],
        [
            {id: 3, orientation: 0},
            {id: 2, orientation: 2},
            {id: 6, orientation: 0}
        ],
    ],
});
// cleanup
afterAll(() => {
    stopMockServer();
    closeClient();
})


describe('delay function', () => {
	// see if sleeps for n milliseconds
	it('basic test', async () => {
		const start = performance.now();
		await delay(100);
		const end = performance.now();
		expect(Math.abs((end - start - 100))).to.be.lessThan(1000);
	});
});

import parseUrl from '../../src/client/utilities/parseUrl.js'
import store from '../../src/client/store/index.js'

global.window = {
	location: {
		hash: ''
	}
}

const throwToMsg = (t) => {
	return `
Exception thrown during player/room resolution.: Error: ${t}
Is the url in the correct format? /#<room>[<player_name>]
window.location.hash: ${window.location.hash ? window.location.hash : '<empty>'}
You will have to explicitly refresh the page after editing the url!
`
}
describe('parse url', () => {
	it('throw errors', async () => {
		global.window.location.hash = 'waw';
		parseUrl();
		let m = store.getState().url.error;
		expect(m).to.equal(
			throwToMsg('Invalid url format for room name')
		);

		global.window.location.hash = '#ds[]';
		parseUrl();
		m = store.getState().url.error;
		expect(m).to.equal(
			throwToMsg('Invalid url format for player name')
		);

		global.window.location.hash = '';
		parseUrl();
		m = store.getState().url.error;
		expect(m).to.equal(
			throwToMsg('Invalid url format for room name')
		);
	});

	it('valid hash', async () => {
		global.window.location.hash = '#waw[uu]';
		parseUrl();
		let u = store.getState().url;
		expect(u.error).to.equal(undefined);
		expect(u.roomName).to.equal('waw');
		expect(u.playerName).to.equal('uu');
	});
});

import {getGreatestPossibleY} from '../../src/client/utilities/input.js'
import {handleKeyUp} from '../../src/client/utilities/input.js'
import {handleKeyDown} from '../../src/client/utilities/input.js'
import {startFall, stopFall} from '../../src/client/utilities/fall.js'


// to stop falling player error
global.setInterval = () => true;

describe('input.js tests', () => {
	it('greatestPossibleY', async () => {
		expect(getGreatestPossibleY()).to.equal(18);
	});
	it('key up', () => {
		handleKeyUp({code: 'ArrowDown'});
		startFall();
		handleKeyUp({code: 'ArrowDown'});
		stopFall();
	});
	it('key down', () => {
		const events = [
			"Space",
			"ArrowUp",
			"ArrowDown",
			"ArrowLeft",
			"ArrowRight"
		]
		for (const i of events) {
			const obj = {
				code: i
			}
			handleKeyDown(i);
			startFall();
			handleKeyDown(i);
			stopFall();
		}
	});
});
