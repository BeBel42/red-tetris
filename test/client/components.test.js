'use strict';

import chai from "chai"
chai.should();

import React from 'react'
import renderer from 'react-test-renderer';
import store from '../../src/client/store/index.js'
import { Provider } from 'react-redux'
import {act} from 'react-dom/test-utils'

import { HomeScreen } from '../../src/client/components/HomeScreen.jsx'

// mock server and socket configs
import { setMockServer, stopMockServer, closeClient } from '../helpers/wsHelpers.js';
// setting server responses
setMockServer({
    nextPiece: [
        [
            {id: 4, orientation: 1},
            {id: 3, orientation: 0},
            {id: 2, orientation: 2}
        ],
        [
            {id: 3, orientation: 0},
            {id: 2, orientation: 2},
            {id: 6, orientation: 0}
        ],
    ],
});
// cleanup
afterAll(() => {
    stopMockServer();
    closeClient();
})


describe('HomeScreen', () => {
	it('Components presence', () => {
		const component = renderer.create(
			<HomeScreen />
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});

import { GameBanner } from '../../src/client/components/GameBanner.jsx'

describe('GameBanner', () => {
	it('Components presence', () => {
		const component = renderer.create(
			<GameBanner />
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});

import GameTitle from '../../src/client/components/GameTitle.jsx'

describe('GameTitle', () => {
	it('Components presence', () => {
		const component = renderer.create(
			<GameTitle />
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});

import { Settings } from '../../src/client/components/Settings.jsx'

describe('Settings', () => {
	it('Components presence', () => {
		const url = {
			roomName: 'eee',
			playerName: 'martin'
		};
		const player = {
			type: 'admin',
		};
		const game = {
			playerList: [{name: 'martin'}, {name: 'david'}, {name: 'pierre'}]
		};
		const component = renderer.create(
			<Provider store={store}>
			<Settings url={url} player={player} game={game}/>
			</ Provider>
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});

import ScoreList from '../../src/client/components/ScoreList.jsx'

describe('ScoreList', () => {
	it('Components presence', () => {
		const component = renderer.create(
			<ScoreList url={{roomName: 'waw'}}/>
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});

import SpectrumGrid from '../../src/client/components/SpectrumGrid.jsx'
describe('SpectrumGrid', () => {
	it('Components presence', () => {
		const board = new Array(10 * 20).fill(0)
		const component = renderer.create(
			<SpectrumGrid url={{roomName: 'room'}} board={board} name='waw'/>
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});


import GameOverStatus from '../../src/client/components/GameOverStatus.jsx'
describe('GameOverStatus', () => {
	it('Components presence', () => {
		const component = renderer.create(
			<Provider store={store}>
			<GameOverStatus />
			</ Provider>
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('Components presence lost true', async () => {
		const game = {
			...store.getState().game,
			lost: true,
			playerList: ['martin']
		}
		const component = renderer.create(
			<Provider store={store}>
			<GameOverStatus game={game} playerName='martin' playerType='admin'/>
			</ Provider>
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});

import { LoadAndLaunch } from '../../src/client/components/LoadAndlaunch.jsx'
import { setJoined } from '../../src/client/store/game.js'
describe('LoadAndLaunch', () => {
	it('Components presence', () => {
		const component = renderer.create(
			<Provider store={store}>
			<LoadAndLaunch />
			</ Provider>
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
	it('Components presence joined', async () => {
		await act(() => {
			store.dispatch(setJoined(true));
		});
		const component = renderer.create(
			<Provider store={store}>
			<LoadAndLaunch />
			</ Provider>
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});


import Game from '../../src/client/components/Game.jsx'
import {stopFall} from '../../src/client/utilities/fall.js'

// to make player move not trigger every second
global.setInterval = () => {}

describe('Game', () => {
	it('Components presence', async () => {
		let component;
		await act(() => {
			component = renderer.create(
				<Provider store={store}>
				<Game />
				</ Provider>
			);
		});
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});

import LostDueToReload from '../../src/client/components/LostDueToReload.jsx'
describe('LostDueToReload', () => {
	it('components rendering', async () => {
		let component;
		await act(() => {
			component = renderer.create(
				<Provider store={store}>
				<LostDueToReload />
				</ Provider>
			);
		});
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});
