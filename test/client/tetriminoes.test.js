import chai, { expect } from "chai"
import {
    defaultTetriminoes,
    getRotatedTetrimino,
    isValidTetrimino,
} from '../../src/client/utilities/tetriminoes.js'
import fetchNewTetrimino from '../../src/client/utilities/fetchNewTetrimino.js'

chai.should()

// mock server and socket configs
import { setMockServer, stopMockServer, closeClient } from '../helpers/wsHelpers.js';
// setting server responses
setMockServer({
    nextPiece: [
        [
            {id: 4, orientation: 1},
            {id: 3, orientation: 0},
            {id: 2, orientation: 2}
        ],
        [
            {id: 3, orientation: 0},
            {id: 2, orientation: 2},
            {id: 6, orientation: 0}
        ],
    ],
});
// cleanup
afterAll(() => {
    stopMockServer();
    closeClient();
})


describe('Tetrimino Rotation', () => {
  it('Three tetris', () => {
    const tetriminoes = [
      defaultTetriminoes[0],
      defaultTetriminoes[3],
      defaultTetriminoes[1],
    ]
    const rotatedGrids = [
      [
        0, 0, 1, 0,
        0, 0, 1, 0,
        0, 0, 1, 0,
        0, 0, 1, 0,
      ],
      [
        1, 1,
        1, 1,
      ],
      [
        0, 1, 1,
        0, 1, 0,
        0, 1, 0,
      ],
    ];
    const widthHeightResults = [4, 2, 3];
    tetriminoes.forEach((t, index) => {
      const rt = getRotatedTetrimino(t, 1);
      rt.width.should.equal(rt.height);
      rt.width.should.equal(widthHeightResults[index]);
      rt.grid.should.deep.equal(rotatedGrids[index]);
    });
  });
});

describe('Tetrimino Validity', () => {
    it('Only Grid', () => {
        const staticTetriminoes = new Array(10 * 20).fill(0);
        const tetrimino = defaultTetriminoes[0];
        let r;
        r = isValidTetrimino(0, 0, tetrimino, staticTetriminoes, true);
        r.should.equal(true);
        r = isValidTetrimino(0, -1, tetrimino, staticTetriminoes, true);
        r.should.equal(true);
        staticTetriminoes[9 * 10 + 3] = 1;
        r = isValidTetrimino(0, 8, tetrimino, staticTetriminoes, true);
        r.should.equal(false);
        r = isValidTetrimino(9, 8, tetrimino, staticTetriminoes, true);
        r.should.equal(true);
        r = isValidTetrimino(1, 20, tetrimino, staticTetriminoes, true);
        r.should.equal(true);
    })

    it('With bounds', () => {
        const staticTetriminoes = new Array(10 * 20).fill(0);
        const tetrimino = defaultTetriminoes[0];
        let r;
        r = isValidTetrimino(-10, -10, tetrimino, staticTetriminoes, false);
        r.should.equal(false);
        r = isValidTetrimino(-10, 1, tetrimino, staticTetriminoes, false);
        r.should.equal(false);
        r = isValidTetrimino(9, 1, tetrimino, staticTetriminoes);
        r.should.equal(false);
    })
});


import store from '../../src/client/store/index.js'
import {setStarted} from '../../src/client/store/game.js'

describe('Fetch new tetrimino', () => {
    it('twice with and without started', async () => {
        await fetchNewTetrimino();
        store.getState().game.stockTetrimino1.id.should.equal(4);
        store.getState().game.stockTetrimino2.orientation.should.equal(0);
        store.getState().game.stockTetrimino3.id.should.equal(2);
        await store.dispatch(setStarted(true));
        await fetchNewTetrimino();
        store.getState().game.started.should.equal(true);
        store.getState().game.stockTetrimino1.id.should.equal(3);
        store.getState().game.stockTetrimino2.orientation.should.equal(2);
        store.getState().game.stockTetrimino3.id.should.equal(6);
    });
});

import getTetriminoColor from '../../src/client/utilities/getTetriminoColor.js';

describe('Tetrimino color', () => {
    it('all cases', async () => {
        for (let i = 0; i < 7; i++)
            switch (i) {
                case 0:
                    expect(2).to.be.equal(getTetriminoColor(i));
                    break;
                case 1:
                    expect(3).to.be.equal(getTetriminoColor(i));
                    break;
                case 2:
                    expect(4).to.be.equal(getTetriminoColor(i));
                    break;
                case 3:
                    expect(5).to.be.equal(getTetriminoColor(i));
                    break;
                case 4:
                    expect(6).to.be.equal(getTetriminoColor(i));
                    break;
                case 5:
                    expect(7).to.be.equal(getTetriminoColor(i));
                    break;
                case 6:
                    expect(8).to.be.equal(getTetriminoColor(i));
                    break;
            }
    });
});
