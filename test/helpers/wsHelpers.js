import { Server } from "socket.io";
import clientSocket from '../../src/client/socket/index.js'

// init server
const io = new Server();
io.listen(3004);

// e.g.:
// setMockServer({
//     start: ['response1', 'response2'],
//     play: [{
//         players: []
//     }],
//     ...
// })
export function setMockServer(responseObjs) {
    const indexes = {};   // index for response mapping

    io.on('connection', client => {
        for (let key in responseObjs) {
            indexes[key] = 0;
            client.on(key, (data, ack) => {
                ack(responseObjs[key][indexes[key]]);
                // to get next response
                indexes[key] = (indexes[key] + 1) % responseObjs[key].length
            });
        }
    });
}

// to avoid open handles in jest
export function stopMockServer() {
    io.close();
}

// closing client handle too
export function closeClient() {
    clientSocket.close();
}
