FROM node:20-alpine3.16

COPY . .

RUN npm i

ENTRYPOINT npm run start

expose 3004
