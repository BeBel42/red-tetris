const path = require('path')
// const CircularDependencyPlugin = require('circular-dependency-plugin')

module.exports = {
    entry: './src/client/index.jsx',
    mode: 'development',
    // plugins: [
    //     new CircularDependencyPlugin({})
    // ],
    devServer: {
        static: {
            directory: path.join(__dirname, 'src/client'),
        },
    },

    output: {
        path: path.join(__dirname, 'build'),
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options:{
                    presets: [
                        "@babel/preset-env",
                        "@babel/preset-react"
                    ]
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            }
        ]
    }

};
