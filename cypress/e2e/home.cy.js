describe('Home creen', () => {
  it('form works', () => {
    // Go to home screen
    cy.visit('http://localhost:3004');
    cy.get("#home-submit").should('be.disabled');
    // enter a room
    cy.get('#home-room').type('cypress');
    cy.get("#home-submit").should('be.disabled');
    cy.get('#home-username').type('cy');
    cy.get("#home-submit").should('be.enabled');

    // load wrong url
    cy.visit('http://localhost:3004/RGGZaz_azfe');
    cy.get("#home-submit").should('be.disabled');
  })
})
