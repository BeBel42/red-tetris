describe('game behavior', () => {
	it('should load a game', () => {
		cy.visit('http://localhost:3004/#cypress[cy]');
		// check title and admin and members size
		cy.log('Check title and admin and members size')
		cy.get('#room-name').should('have.text', 'cypress');
		cy.get('#player-name').should('have.text', 'cy');
		cy.get('#other-players').children().should('have.length', 1);
		// start game
		cy.log('Start game')
		cy.get('#start-game').click();
		// loose a game
		cy.log('Loose a game')
		for (let i = 0; i < 15; i++)
			cy.get('#grid-id', {log: false}).type(' ', {log: false});
		cy.get('#game-over').should('be.visible');
		// restart the game
		cy.log('Restart the game')
		cy.get('#restart-game').click();
		// refresh the page
		cy.log('Refresh the page')
		cy.get('#room-name').should('have.text', 'cypress');
		cy.get('#start-game').click();
		cy.reload();
		// check that I lost
		cy.log('Check that I lost')
		cy.get('#lost-for-reload').should('be.visible');
		// restart game to come back to a neutral state
		cy.log('restart game to come back to a neutral state')
		cy.get('#restart-game').click();
	});
});
