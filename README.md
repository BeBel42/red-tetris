# Red Tetris
### This site is [available online!](https://red-tetris.mlefevre.xyz/)  

![Scores list of Red Tetris](https://gitlab.com/BeBel42/red-tetris/-/raw/master/screenshots/scores.png)
![Game of Red Tetris](https://gitlab.com/BeBel42/red-tetris/-/raw/master/screenshots/game.png)

Red Tetris is a project from [campus 19](https://campus19.be/).  
It consists of creating a Full Stack multiplayer Tetris game in React.
React, Redux, Express, and MongoDB were used for this project.

## Technical details
### Commands
- `npm run start` to build and launch red tetris
- `npm install` to build red-tetris
- `npm run test` to test red-tetris
- `npm run cypress` to test red-tetris with cypress
- `npm run coverage` to run a coverage analysis of red-tetris

### Kubernetes compatibility
Red Tetis stores its scores in a remote MongoDB database.  
Multiplayer games are managed by the server - it is not stateless.
Thus, red-tetris can be deployed to Kubernetes **but only with one pod**.
