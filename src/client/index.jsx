import React from 'react'
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux'
import store from './store/index.js'
import LoadAndLaunch from './components/LoadAndlaunch.jsx'
import parseUrl from './utilities/parseUrl.js'
import initSocket from './socket/listenToEvent.js'

// genrerating main component and passing redux store to it
const root = createRoot(document.getElementById("root"));
root.render((
    <Provider store={store}>
	<LoadAndLaunch />
    </Provider>
))

// laucnhes default functions
function init() {
    // putting url params in redux store
    parseUrl();
    initSocket();
}

init();

export default root
