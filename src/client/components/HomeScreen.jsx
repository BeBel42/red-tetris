'use strict';
import {useEffect, useState} from 'react'
import React from 'react'
import {connect} from 'react-redux'
import ScoreList from './ScoreList.jsx'

// url error css
const errorStyle = {
	textAlign: 'center',
	fontWeight: 'bold',
	whiteSpace: 'pre-line',
	color: '#ff7777',
	fontSize: '1.5em'
}

// Will display home screen if url is wrong and a form to join a new room
export function HomeScreen(props) {
	const [room, setRoom] = useState("");
	const [pseudo, setPseudo] = useState("");
	const [isValid, setValid] = useState(false);

	const validate = () => {
		return room.length && pseudo.length;
	};

	useEffect(() => {
		const isValid = validate();
		setValid(isValid);
	}, [room, pseudo]);

	const handleSubmit = (event) => {
		event.preventDefault();
		window.open(`/#${room}[${pseudo}]`, '_parent');
		window.location.reload();
	};

	return (
		<div>

	<div style={{
		fontSize: '2em',
		color: 'gold',
		textDecorationLine: 'underline',
		display: 'flex',
		justifyContent: 'center',
	}}>RED TETRIS</div>
		<form onSubmit={handleSubmit}>
		<div style={{display: 'flex', justifyContent: 'center'}}>
		<div style={{margin: '5px'}}>
		<label>Enter the room name:
		<input
			style={{marginTop: '4px'}}
			id="home-room"
			type="text"
			value={room}
			onChange={(e) => setRoom(e.target.value)}
			/>
		</label>
		</div>
		<div style={{margin: '5px'}}>
		<label>Enter your pseudo:
		<input
			style={{marginTop: '4px'}}
			id="home-username"
			type="text"
			value={pseudo}
			onChange={(e) => setPseudo(e.target.value)}
			/>
		</label>
		</div>
		</div>
		<div style={{display: 'flex', justifyContent: 'center'}}>
		<input id="home-submit" type="submit" disabled={!isValid} />
		</div>
		</form>
		<hr/>
		<ScoreList />
		</div>
	);
}

// passing some variables to render
const mapStateToProps = state => {
	return {
		error: state.url.error,
	}
}
export default connect(mapStateToProps)(HomeScreen);
