'use strict';

import React, { useState } from 'react'
import {connect} from 'react-redux'
import {setGravity} from '../store/game.js'
import store from '../store/index.js'

// pretty css
const sliderStyle = {
	all: 'initial',
	width: '50%',
	height: '15px',
	background: '#000',
	outline: 'none',
	border: '5px solid gray',
	borderRadius: '8px',
}

// slider in settings menu
function GravitySlider({gravity}) {
	return <>
	<div>Gravity: {gravity}</div>
	<input
	style={sliderStyle}
	type='range'
	min='1'
	max='10'
	defaultValue={gravity}
	onChange={(e) => store.dispatch(setGravity(e.target.value))}
		/>
	</>
}

// passing some variables to render
const mapStateToProps = state => {
	return {
		gravity: state.game.gravity,
	}
}
export default connect(mapStateToProps)(GravitySlider);
