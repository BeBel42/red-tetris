'use strict';

import React from 'react'
import {useEffect} from 'react'
import Grid from './Grid.jsx'
import {handleKeyDown, handleKeyUp} from '../utilities/input.js'
import '../styles/disableOutline.css' // disabling annoying focus outline
import GameBanner from './GameBanner.jsx'
import store from '../store/index.js'
import {newPlayerTetrimino} from '../store/player.js'
import {startFall} from '../utilities/fall.js'
import Spectrum from './Spectrum.jsx'
import GameTitle from './GameTitle.jsx'
import GameOverStatus from './GameOverStatus.jsx'
import NextPieceGrid from './NextPieceGrid.jsx'

// main component
export default function Game() {
	// for autofocus
	const focusRef = React.useRef();
	useEffect(() => {
		// setting player tetrimino
		store.dispatch(newPlayerTetrimino());
		// begining player falling
		const time = 1000 / store.getState().game.gravity;
		startFall(time);
		// Autofocus the rendered div
		focusRef.current?.focus();
	}, [])


	// rendering the page
	return <>
	<GameBanner />
	<div
	ref={focusRef}
	tabIndex="-1"
	onKeyDown={(e) => handleKeyDown(e)}
	onKeyUp={(e) => handleKeyUp(e)}
	>
	<Grid />
	</div>
	<NextPieceGrid />
	<GameTitle />
	<Spectrum />
	<GameOverStatus />
	</>;
}
