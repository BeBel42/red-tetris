'use strict';

import React from 'react'
import '../styles/grid.css'
import {connect} from 'react-redux'
import { defaultTetriminoes } from '../utilities/tetriminoes.js';
import getTetriminoColor from '../utilities/getTetriminoColor';
import { getRotatedTetrimino } from '../utilities/tetriminoes.js';

// to map staticTetriminoes's values to color
const caseClasses = [
	'background',
	'white',
	'cyan',
	'blue',
	'orange',
	'yellow',
	'green',
	'magenta',
	'red',
	'black',
]

// main tetris grid
function NextPieceGrid(props) {
	const cases = new Array(16).fill(0).map(
		(x, index) => <span key={`${index}`} className={'background'}></span>
	);
	if (props.tetrimino === undefined)
		return <div id='next-piece-grid-id'>{cases}</div>

	// getting tetrimino grid and color
	const tetrimino = getRotatedTetrimino(
		defaultTetriminoes[props.tetrimino.id],
		props.tetrimino.orientation
	);
	const color = caseClasses[getTetriminoColor(props.tetrimino.id)];

	// painting grid
	for (let y = 0; y < tetrimino.height; y++) {
		for (let x = 0; x < tetrimino.width; x++) {
			if (!tetrimino.grid[y * tetrimino.width + x])
				continue;
			cases[y * 4 + x] = <span
			className={color} key={`line-${y}-column-${x}`}>
			<div> </div>
			</span>
		}
	}
	return <div id='next-piece-grid-id'>{cases}</div>
}

// I need to use that so I don't use useState() in render() (I cannot)
const mapStateToProps = state => {
     return {
		 tetrimino: state.game.stockTetrimino2,
     }
}
export default connect(mapStateToProps)(NextPieceGrid);
