'use strict';

import React from 'react'
import SpectrumGrid from './SpectrumGrid.jsx'
import { connect } from 'react-redux'

// spectrum css
const mainStyle = {
	position: 'fixed',
	top: '6vh',
	right: '2vw',
	backgroundColor: '#303030',
	display: 'grid',
	gridTemplateColumns: 'auto auto auto',
	gap: '3px',
};

// diplaying all spectrum grids at the right
function Spectrum(props) {
	const grids = props.game.playerList
					   .filter(i => i.name !== props.playerName)
					   .map((i, index) => {
						   return <SpectrumGrid
						   key={index}
						   name={i.name}
						   board={i.board}
							   />}
					   );
	return <div style={mainStyle}>
	{grids}
	</div>
}

// passing some variables to render
const mapStateToProps = state => {
	return {
		game: state.game,
		playerName: state.url.playerName,
	}
}
export default connect(mapStateToProps)(Spectrum);
