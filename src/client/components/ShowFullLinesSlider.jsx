'use strict';

import React from 'react'
import {connect} from 'react-redux'
import {setShowFullLines} from '../store/game.js'
import store from '../store/index.js'

// pretty css
const sliderStyle = {
	all: 'initial',
	width: '50%',
	height: '15px',
	background: '#000',
	outline: 'none',
	border: '5px solid gray',
	borderRadius: '8px',
}

// slider in settings menu
function ShowFullLinesSlider({showFullLines}) {
	return <>
	<div>ShowFullLines: {showFullLines ? 'On' : 'Off'}</div>
	<input
	style={sliderStyle}
	type='range'
	min='0'
	max='1'
	defaultValue={Number(showFullLines)}
	onChange={(e) =>
		store.dispatch(setShowFullLines(Boolean(Number(e.target.value))))
	}
		/>
	</>
}

// passing some variables to render
const mapStateToProps = state => {
	return {
		showFullLines: state.game.showFullLines,
	}
}
export default connect(mapStateToProps)(ShowFullLinesSlider);
