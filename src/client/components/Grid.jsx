import React from 'react'
import '../styles/grid.css'
import {connect} from 'react-redux'
import {handleStaticTetriminoesChange} from '../utilities/staticTetriminoes.js'
import { useEffect } from "react";
import usePrevious from '../utilities/usePrevious.js'

// to map staticTetriminoes's values to color
const caseClasses = [
	'background',
	'white',
	'cyan',
	'blue',
	'orange',
	'yellow',
	'green',
	'magenta',
	'red',
	'black',
]

// returns whether the x-y coordinates are in a non-empty player cell
function shouldColorPlayer(x, y, player)
{
	const indexInPlayer = (y - player.y)
		  * player.tetrimino.width
		  + (x - player.x);
	return (y >= player.y
			&& y < player.y + player.tetrimino.height
			&& x >= player.x
			&& x < player.x + player.tetrimino.width
			&& player.tetrimino.grid[indexInPlayer] !== 0)
}

// main tetris grid
const Grid = (props) => {
	// handle when staticTetriminoes change
	const currentStaticTetriminoes = props.grid.staticTetriminoes;
	const previousStaticTetriminoes = usePrevious(currentStaticTetriminoes);
	useEffect(() => {
		if (previousStaticTetriminoes !== currentStaticTetriminoes)
			handleStaticTetriminoesChange();
	}, [currentStaticTetriminoes])

	// retreiving store vars
	const grid = props.grid;
	const player = props.player;

	// painting grid
	const cases = []
	for (let y = 0; y < 20; y++) {
		for (let x = 0; x < 10; x++) {
			let color = caseClasses[grid.staticTetriminoes[10 * y + x]];
			if (shouldColorPlayer(x, y, player)) // case taken by player
				color = caseClasses[player.color];
			cases.push(<span
					   className={color} key={`line-${y}-column-${x}`}>
					   <div> </div>
					   </span>)
		}
	}
	return <div id='grid-id'>{cases}</div>
}

// I need to use that so I don't use useState() in render() (I cannot)
const mapStateToProps = state => {
     return {
         player: state.player,
         grid: state.grid
     }
}
export default connect(mapStateToProps)(Grid);
