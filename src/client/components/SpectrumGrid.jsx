'use strict';

import React from 'react'
import store from '../store/index.js'

const css = {
	display: 'grid',
	gridTemplateColumns: 'repeat(10, min(0.8vh, 1vw))',
	gridTemplateRows: 'repeat(20, min(0.8vh, 1vw))',
}
const empty = {
	backgroundColor: '#444444',
};
const full = {
	backgroundColor: '#777777',
};

// small grid for each player spectra
export default function SpectrumGrid({ board, name }) {
	const copy = JSON.parse(JSON.stringify(board));
	const showFullLines = store.getState().game.showFullLines;
	// to only show last line
	if (!showFullLines) {
		let onFirstLine = false;
		let passedFirstLine = false;
		// coloring rows that are not first line
		for (const [index, i] of copy.entries()) {
			if (passedFirstLine) {
				copy[index] = 1;
				continue;
			}
			if (onFirstLine && index % 10 === 0) {
				passedFirstLine = true;
				copy[index] = 1;
				continue;
			}
			if (i) onFirstLine = true;
		}
	}
	// from number to html
	const gridElems = copy.map((i, index) => {
		const c = (i ? full : empty);
		return <span style={c} key={index}></span>
	});
	return <div>
	<u>{name}</u>
	<div style={css}>
	{gridElems}
	</div>
	</div>
}
