'use strict';

import React from 'react'
import { useState } from 'react'
import socket from '../socket/index.js'
import { useEffect } from 'react';
import {sendSocket, listenSocket} from '../store/socket.js'
import store from '../store/index.js'


// put score lists side by side
const flex = {
	display: 'flex',
	justifyContent: 'space-around',
	textAlign: 'right'
};

// margin for tables
const margin = {
	marginLeft: '10px',
	marginRight: '10px'
}

// return two list of scores - latest and best
export default function ScoreList() {
	// to know when to stop displaying loading message
	const [smtLoaded, setSmtLoaded] = useState(false);
	// decalring lists
	const [top10, setTop10] = useState([])
	const [last10, setLast10] = useState([])
	// fetching data
	useEffect(() => {
		store.dispatch(sendSocket('top10Score', undefined, (scores) => {
			const l = [];
			for (const [index, i] of scores.entries()) {
				l.push(<tr key={index}><td>{i.username}</td><td>{i.score}</td></tr>)
			}
			setSmtLoaded(true);
			setTop10(l);
		}));
		store.dispatch(sendSocket('last10Score', undefined, (scores) => {
			const l = [];
			for (const [index, i] of scores.entries()) {
				l.push(<tr key={index}><td>{i.username}</td><td>{i.score}</td></tr>)
			}
			setSmtLoaded(true);
			setLast10(l);
		}));
	}, []);
	// loading message
	if (!smtLoaded)
		return <div style={flex}><strong>Loading Score List...</strong></div>
	// renderung elem
	return <>
	<h2 style={{textAlign: 'center'}}>Scores</h2>
	<div style={flex}>
	<table style={margin}>
	<caption style={{textAlign: 'center'}}><u>Top 10</u></caption>
	<thead><tr><th>Name</th><th>Score</th></tr></thead>
	<tbody>
	{top10}
	</tbody>
	</table>
	<table style={margin}>
	<caption style={{textAlign: 'center'}}><u>10 Most Recent</u></caption>
	<thead><tr><th>Name</th><th>Score</th></tr></thead>
	<tbody>
	{last10}
	</tbody>
	</table>
	</div>
	</>
}
