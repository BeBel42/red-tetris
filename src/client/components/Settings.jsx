'use strict';

import socket from '../socket/index.js'
import React from 'react'
import {connect} from 'react-redux'
import store from '../store/index.js'
import {setStarted} from '../store/game.js'
import Game from '../components/Game.jsx'
import LostDueToReload from '../components/LostDueToReload.jsx'
import GravitySlider from './GravitySlider.jsx'
import ShowFullLinesSlider from './ShowFullLinesSlider.jsx'
import {sendSocket, listenSocket} from '../store/socket.js'

// css styles
const centerStyle = {
	textAlign: 'center'
};
const flexStyle = {
	display: 'flex',
	justifyContent: 'space-around',
	position: 'relative',
	marginTop: '10%',
};
const waitingStyle = {
	...centerStyle,
	position: 'fixed',
	top: '85vh',
	left: '50%',
	color: 'gray',
	transform: 'translate(-50%, -50%)',
};
const buttonStyle = {
	...centerStyle,
	position: 'fixed',
	top: '85vh',
	left: '50%',
	transform: 'translate(-50%, -50%)',
};
const boxStyle = {
	...flexStyle,
	backgroundColor: '#550077',
	height: '50vh',
	borderStyle: 'solid',
	borderWidth: '5px',
};
const otherPlayersStyle = {
	display: 'grid',
	gridTemplateColumns: 'auto auto',
}

// other player list
function OtherPlayers(props) {
	const jsxArray = props.playerList.map((i, index) => {
		const st = (i.name === props.playerName ? {color: 'cyan'} : {color: '#bbff99'})
		return <div key={index} style={st}>{i.name}</div>
	})
	return <div style={centerStyle}>
	<h2><u>Room members</u></h2>
	<div style={otherPlayersStyle} id="other-players">
	{jsxArray}
	</div>
	</div>
}

// change state when button is pressed
function buttonPressed() {
	const g = store.getState().game;
	const obj = {
		gravity: g.gravity,
		showFullLines: g.showFullLines,
	};
	store.dispatch(sendSocket('start', obj));
	store.dispatch(setStarted(true));
}

// button that starts a game when you are admin
function SubmitButton(props) {
	return <button
		id='start-game'
		style={buttonStyle}
		onClick={buttonPressed}
		type="button">
		{props.isSolo ? "Play alone" : "Let's go!"}
	</button>
}

// settings page
export function Settings(props) {
	if (store.getState().game.lostDueToReload)
		return <LostDueToReload/>
	if (store.getState().game.started)
		return <Game />
	return <div>
	<h1 style={centerStyle}><u id="room-name">{props.url.roomName}</u></h1>
	<div style={boxStyle}>
	<div style={centerStyle}>
	<h2><u>You</u></h2>
	<span style={{color: 'cyan'}} id="player-name">{props.url.playerName}</span> ({props.player.type})
	{props.player.type === 'admin' &&
	 <>
	 <br /> <br />
	 <GravitySlider />
	 <ShowFullLinesSlider />
	 </>
	}
	</div>
	<OtherPlayers playerList={props.game.playerList} playerName={props.url.playerName}/>
	</div>
	{props.player.type === 'admin' ||
	 <h1 style={waitingStyle}>Waiting for admin...</h1>}
	{props.player.type === 'admin' &&
	 <SubmitButton isSolo={props.game.playerList.length === 1}/>}
	</div>
}

// passing some variables to render
const mapStateToProps = state => {
	return {
		game: state.game,
		player: state.player,
		url: state.url,
	}
}
export default connect(mapStateToProps)(Settings);
