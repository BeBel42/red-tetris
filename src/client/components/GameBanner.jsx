'use strict';

import {connect} from 'react-redux'
import React from 'react'

// banner CSS
const scoreStyle = {
	fontSize: '4vh',
	color: 'orange',
	fontWeight: 'bold'
};
const playerTypeStyle = {
	fontSize: '4vh',
	color: '#9999ff',
	marginRight: '10px',
};
const playerPlayingStyle = {
	fontSize: '4vh',
	color: '#ccffcc'
};
const playerLostStyle = {
	fontSize: '4vh',
	color: 'red'
};

// return top banner containing basic session info
export function GameBanner(props) {
	return <div style={{
		display: 'flex',
		justifyContent: 'space-between',
		backgroundColor: '#222222',
		width: '100vw',
		height: '5vh',
		position: 'fixed',
		top: 0,
		right: 0,
	}}>
	<span style={scoreStyle}>{props.score}</span>
	<span>
	<span style={playerTypeStyle}>{props.playerType}</span>
	{props.lost
	 && <span style={playerLostStyle} id="game-over">Game Over</span>
	 || <span style={playerPlayingStyle}>Playing</span>}
	</span>
	</div>
}


// passing some variables to render
const mapStateToProps = state => {
     return {
         score: state.game.score,
         lost: state.game.lost,
         playerType: state.player.type,
     }
}
export default connect(mapStateToProps)(GameBanner);
