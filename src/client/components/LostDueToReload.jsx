'use strict';

import React from 'react';
import GameOverStatus from './GameOverStatus.jsx'

// Message displayed in case of an unexpected disconnect
export default function LostDueToReload() {
	return <>
	<div id="lost-for-reload">
	You have quit the game unexpectedly. <br/>
	This is considered as a loss.
	</div>
	<GameOverStatus />
	</>
}
