'use strict';

import React from 'react'

// Game title at the middle of the screen
export default function GameTitle() {
	const titleStyle = {
		fontSize: '2em',
		color: 'gold',
		textDecorationLine: 'underline',
		position: 'fixed',
		top: '45vh',
		left: '45vw',

	}
	return <span style={titleStyle}>RED TETRIS</span>
}
