'use strict';
import socket from '../socket/index.js'
import {setPlayerType} from '../store/player.js'
import {setPlayerList} from '../store/game.js'
import {setJoined} from '../store/game.js'
import store from '../store/index.js'
import React from 'react'
import Settings from './Settings.jsx'
import {connect} from 'react-redux'
import HomeScreen from "./HomeScreen.jsx";
import {sendSocket, listenSocket} from '../store/socket.js'
import '../styles/loading.css'


// Displays loading animation
function Loading() {
	return <>
	<div className="center">
	<div className="wave"></div>
	<div className="wave"></div>
	<div className="wave"></div>
	<div className="wave"></div>
	<div className="wave"></div>
	<div className="wave"></div>
	<div className="wave"></div>
	<div className="wave"></div>
	<div className="wave"></div>
	<div className="wave"></div>
	</div>
	</>
}

// Will display loading screen until connected (or display error if url is invalid)
export function LoadAndLaunch(props) {
	// set join state to true to launch game
	socket.on('connect', () => {
		if (props.error) return // url params are invalid
		// joining game
		const payload = {playerName: props.playerName, roomName: props.roomName};
		store.dispatch(sendSocket(
			"join",
			payload,
			(playerType, playerList) => {
				// setting session data
				store.dispatch(setPlayerList(playerList));
				store.dispatch(setPlayerType(playerType));
				store.dispatch(setJoined(true));
			}
		));
	});
	// displays url format error
	if (props.error)
		return <HomeScreen />
	// displays app or loading screen
	return (props.joined
			&& <Settings />
			|| <Loading />)
}

// passing some variables to render
const mapStateToProps = state => {
	return {
		joined: state.game.joined,
		playerName: state.url.playerName,
		roomName: state.url.roomName,
		error: state.url.error,
	}
}
export default connect(mapStateToProps)(LoadAndLaunch);
