'use strict';

import React from 'react';
import socket from '../socket/index.js';
import { connect } from 'react-redux';
import store from '../store/index.js';
import { restartGameSession } from '../store/game.js';

// main css
const textStyle = {
	fontSize: '2em',
	position: 'fixed',
	bottom: '5vh',
	left: '2vw',
	backgroundColor: '#222',
	border: '2px solid #eee',
	padding: '10px',
}

// restart game button
const buttonStyle = {
	fontSize: '0.5em',
}
// button onclick event hanler
function restartGame() {
	store.dispatch(restartGameSession())
}
// button component
function RestartButton() {
	return <button id="restart-game" style={buttonStyle} onClick={restartGame}>Restart Game</button>
}

// small message indimating what to wait for when player lost game
function GameOverStatus(props) {
	// do not display anything if game not stopped
	if (!props.game.winner
		&& !props.game.lost
		&& !props.game.lostDueToReload
	   ) return <></>
	// button and textBox content
	let text = '';
	let restartGameMsg = 'Waiting for admin to restart game...';
	// displaying restart button if player is admin
	if (props.playerType === 'admin')
		restartGameMsg = <RestartButton />;
	if (props.game.winner === props.playerName)
		text = 'You won!';
	else if (props.game.winner)
		text = `${props.game.winner} won!`;
	else if (props.game.lost) {
		if (props.game.playerList.length === 1) // single player
			text = `Game over with score ${props.game.score}`;
		else {					// mutliplayer
			text = 'Waiting for game to finish...';
			restartGameMsg = '';
		}
	}
	return <span style={text ? textStyle : {}}>{text} {restartGameMsg}</span>
}

const mapStateToProps = state => {
	return {
		game: state.game,
		playerName: state.url.playerName,
		playerType: state.player.type,
	}
}
export default connect(mapStateToProps)(GameOverStatus);
