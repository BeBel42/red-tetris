'use strict';

import { setPlayerList } from '../store/game.js'
import { setPlayerType } from '../store/player.js'
import { addPenalty } from '../store/grid.js'
import store from '../store/index.js'
import { setOtherPlayerGrid } from '../store/game.js'
import { setStarted } from '../store/game.js'
import { setWinner } from '../store/game.js'
import { stopFall } from '../utilities/fall.js';
import { lostDueToReload } from '../store/game.js'
import { setGravity, setShowFullLines } from '../store/game.js'
import { listenSocket } from '../store/socket.js'

// all socket event listeners will be put here
export default function listenToEvents() {
	// listening to waiting player list change
	store.dispatch(listenSocket('playerListUpdate', playerList => {
		store.dispatch(setPlayerList(playerList.players))
	}))
	// listening to penalty notification
	store.dispatch(listenSocket('addLines', obj => {
		if (obj.number === 0) return; // no need to add a line
		store.dispatch(addPenalty(obj.number))
	}))
	// listening spectra update
	store.dispatch(listenSocket('updateBoards', obj => {
		store.dispatch(setOtherPlayerGrid(obj));
	}))
	// to know which player won
	store.dispatch(listenSocket('hasWon', playerName => {
		stopFall(); // game ends
		store.dispatch(setWinner(playerName))
	}))
	// get notified when game has started
	store.dispatch(listenSocket('gameStarted', (settings) => {
		if (settings?.gravity)	// to not crash if not implemented in server
			store.dispatch(setGravity(settings.gravity));
		if (settings?.showFullLines)	// to not crash if not implemented in server
			store.dispatch(setShowFullLines(settings.showFullLines));
		store.dispatch(setStarted(true));
	}))
	// admin wants to restart game
	store.dispatch(listenSocket('gameRestarted', () => {
		window.location.reload();
	}))
	// you refreshed page during game - server tells you that you lost
	store.dispatch(listenSocket('lost', () => {
		store.dispatch(lostDueToReload());
	}))
	// player should now be admin
	store.dispatch(listenSocket('admin', (playerList) => {
		store.dispatch(setPlayerList(playerList));
		store.dispatch(setPlayerType('admin'));
	}))
}
