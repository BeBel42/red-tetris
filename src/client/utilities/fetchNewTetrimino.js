'use strict';

import store from '../store/index.js'
import {setStockTetriminoes} from '../store/game.js'
import { sendSocket } from '../store/socket.js';

// fetches next tetriminoes and put them in store
export default function fetchNewTetrimino() {
	return new Promise((resolve) => {
		if(!store.getState().game.started)
			console.warn('Warning: fetching new piece while game is not started')
		store.dispatch(sendSocket('nextPiece', undefined, (res) => {
			store.dispatch(setStockTetriminoes(...res));
			resolve()
		}));
	})
}
