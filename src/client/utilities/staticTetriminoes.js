'use strict';

import store from '../store/index.js'
import {startFall, stopFall} from './fall.js'
import {setPlayerPos} from '../store/player.js'
import {setScore} from '../store/game.js'
import {setWhiteLines, destroyWhiteLines} from '../store/grid.js'
import delay from './delay.js'
import socket from '../socket/index.js'
import { sendSocket } from '../store/socket.js';

// returns array of full lines index
function getFullLines(staticTetriminoes) {
	const fullLines = []
	for (let y = 0; y < 20; y++) {
		for (let x = 0; x < 10; x++) {
			// not a full line if empty or penalty line
			if (staticTetriminoes[y * 10 + x] === 0
				|| staticTetriminoes[y * 10 + x] === 9)
				break;
			if (x === 9)
				fullLines.push(y)
		}
	}
	return fullLines;
}
// to see if change occured from handleStaticTetriminoesChange
let handlingChanges = false;
// to see if change is penalty or not
let previousGrid = new Array().fill(0);

export async function handleStaticTetriminoesChange() {
	// notifying server about grid change
	store.dispatch(sendSocket('updateInfos', {
		board: store.getState().grid.staticTetriminoes,
		score: store.getState().game.score
	}));
	// checking if change is just penalty or not
	const prevBlacks = previousGrid.filter(x => x === 9).length
	const newBlacks = store.getState()
						   .grid.staticTetriminoes.filter(x => x === 9).length
	// updating grid after calculating n of black cases
	previousGrid = JSON.parse(JSON.stringify(
		store.getState().grid.staticTetriminoes
	));
	// doing nothing if change is penalty
	if (prevBlacks !== newBlacks)
		return;
	// change occurred from here - do nothing
	if (handlingChanges) return;
	// stopping game / player fall
	handlingChanges = true;
	stopFall();
	store.dispatch(setPlayerPos(store.getState().player.x, -10));
	let combo = 0;
	let linesRemoved = 0;
	while (true) {
		const fullLines = getFullLines(store.getState().grid.staticTetriminoes);
		if (fullLines.length === 0)
			break;
		store.dispatch(setWhiteLines(fullLines)); // coloring full lines
		// waiting a bit before detroying them
		await delay(1000 / store.getState().game.gravity);
		store.dispatch(destroyWhiteLines());
		// for score computation
		linesRemoved += fullLines.length;
		combo++;
	}
	// getting and setting score
	const addScore = (combo <= 1 ? 0 : combo) * 5 + linesRemoved * 10;
	const newScore = store.getState().game.score + addScore;
	store.dispatch(setScore(newScore))
	store.dispatch(setPlayerPos(store.getState().player.x, 0));
	store.dispatch(sendSocket('updateInfos', {
		score: newScore
	}));
	// sending event ot server
	if (linesRemoved > 0)
		store.dispatch(sendSocket('lineCleaned', linesRemoved));
	// resume game
	startFall(1000 / store.getState().game.gravity);
	handlingChanges = false;
}
