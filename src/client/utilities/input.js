import store from '../store/index.js'
import {rotatePlayer} from '../store/player.js'
import {movePlayer} from '../store/player.js'
import {setPlayerPos} from '../store/player.js'
import {setGravity} from '../store/game.js'
import {startFall, stopFall, isFalling} from './fall.js'
import {addStaticTetrimino} from '../store/grid.js'
import {newPlayerTetrimino} from '../store/player.js'
import {defaultTetriminoes} from './tetriminoes.js'

import {addPenalty} from '../store/grid.js'

// to get Y position of piece if we want to land it (SPC)
export function getGreatestPossibleY() {
	const player = store.getState().player;
	const staticTetriminoes = store.getState().grid.staticTetriminoes;
	let greatestY = 20;
	for (let py = 0; py < player.tetrimino.height; py++)
		for (let px = 0; px < player.tetrimino.width; px++) {
			if (player.tetrimino.grid[py * player.tetrimino.height + px] === 0)
				continue;
			const posx = px + player.x;
			const posy = py + player.y;
			let i;
			for (i = posy; i < 20; i++)
				if (staticTetriminoes[i * 10 + posx] !== 0)
					break;
			const toAdd = (i - 1) - posy;
			const localGreatestY = player.y + toAdd;
			greatestY = (localGreatestY < greatestY ? localGreatestY : greatestY);
		}
	return greatestY;
}

// handles key down input and dispatches event functions
export function handleKeyDown(e) {
	switch(e.code) {
	  case "Space":
		  if (!isFalling()) break;
		  if (e.repeat) break;
		  const greatestY = getGreatestPossibleY();
		  store.dispatch(setPlayerPos(store.getState().player.x, greatestY));
		  const player = store.getState().player;
		  store.dispatch(addStaticTetrimino(
			  player.tetrimino,
			  0,
			  player.x,
			  player.y,
			  player.color
		  ));
		  store.dispatch(newPlayerTetrimino());
		  break;
	  case "ArrowUp":
		  if (!isFalling()) break;
		  if (e.repeat) break;
		  if (e.shiftKey)
			  store.dispatch(rotatePlayer(1))
		  else
			  store.dispatch(rotatePlayer(-1))
		  break;
	  case "ArrowDown":
		  if (!isFalling()) break;
		  if (e.repeat) break;
		  stopFall();
		  const time = 1000 / (store.getState().game.gravity * 5);
		  startFall(time);
		  break;
	  case "ArrowLeft":
		  if (!isFalling()) break;
		  store.dispatch(movePlayer(-1, 0));
		  break;
	  case "ArrowRight":
		  if (!isFalling()) break;
		  store.dispatch(movePlayer(1, 0));
		  break;
	}
}

// handles key upinput and dispatches event functions
export function handleKeyUp(e) {
	switch(e.code) {
	  case "ArrowDown":
		  if (!isFalling()) break;
		  stopFall();
		  const time = 1000 / store.getState().game.gravity;
		  startFall(time);
		  break;
	}
}
