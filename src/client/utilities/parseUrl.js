'use strict';

import store from '../store/index.js'
import {setPlayerName} from '../store/url.js'
import {setRoomName} from '../store/url.js'
import {setError} from '../store/url.js'

// retreives player and room names from url
export default function parseUrl() {
	let roomName, playerName, error;
	try {
		const extractedRoomName = window.location.hash.match(/^\#.+?\[/);
		if (!extractedRoomName)
			throw new Error('Invalid url format for room name');
		const extractedPlayerName = window.location.hash.match(/\[.+\]$/);
		if (!extractedPlayerName)
			throw new Error('Invalid url format for player name');
		const roomNameTmp = extractedRoomName[0]
			  .substring(1, extractedRoomName[0].length - 1)
		const playerNameTmp = extractedPlayerName[0]
			  .substring(1, extractedPlayerName[0].length - 1)
		roomName = roomNameTmp;
		playerName = playerNameTmp;
	} catch(e) {
		const errorMessage = `
Exception thrown during player/room resolution.: ${e}
Is the url in the correct format? /#<room>[<player_name>]
window.location.hash: ${window.location.hash ? window.location.hash : '<empty>'}
You will have to explicitly refresh the page after editing the url!
`;
		error = errorMessage;
	}
	// updating store
	store.dispatch(setRoomName(roomName));
	store.dispatch(setPlayerName(playerName));
	store.dispatch(setError(error));
}
