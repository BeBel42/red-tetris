// list containing all required tetriminoes' data
export const defaultTetriminoes =  [
	{
		width: 4,
		height: 4,
		grid: [
			0, 0, 0, 0,
			1, 1, 1, 1,
			0, 0, 0, 0,
			0, 0, 0, 0,
		]
	},
	{
		width: 3,
		height: 3,
		grid: [
			1, 0, 0,
			1, 1, 1,
			0, 0, 0,
		]
	},
	{
		width: 3,
		height: 3,
		grid: [
			0, 0, 1,
			1, 1, 1,
			0, 0, 0,
		]
	},
	{
		width: 2,
		height: 2,
		grid: [
			1, 1,
			1, 1,
		]
	},
	{
		width: 3,
		height: 3,
		grid: [
			0, 1, 1,
			1, 1, 0,
			0, 0, 0,
		]
	},
	{
		width: 3,
		height: 3,
		grid: [
			0, 1, 0,
			1, 1, 1,
			0, 0, 0,
		]
	},
	{
		width: 3,
		height: 3,
		grid: [
			1, 1, 0,
			0, 1, 1,
			0, 0, 0,
		]
	},
]

// return tetrimino with grid rotated by n levels clockwise
export function getRotatedTetrimino(tetrimino, n) {
	const newGrid = new Array(tetrimino.width * tetrimino.height)
	const xOrigin = tetrimino.width / 2;
	const yOrigin = tetrimino.height / 2;
	for (let aY = 0; aY < tetrimino.height; aY++) // absolute y
		for (let aX = 0; aX < tetrimino.width; aX++) { // absolute x
			const rX = aX - xOrigin + .5; // relative x
			const rY = aY - yOrigin + .5; // relative y
			const rotX = Math.floor(
				(rX * Math.cos(Math.PI * n / 2)
				 - rY * Math.sin(Math.PI * n / 2))
					+ xOrigin
			);
			const rotY = Math.floor(
				(rX * Math.sin(Math.PI * n / 2)
				 + rY * Math.cos(Math.PI * n / 2))
					+ yOrigin
			);
			newGrid[rotY * tetrimino.width + rotX] = tetrimino.grid[aY * tetrimino.width + aX]
		}
	return {					// returning tetrimino object
		width: tetrimino.width,
		height: tetrimino.height,
		grid: newGrid
	}
}

// checks if tetrimino is out of bounds or if it overlaps
// with static tetriminoes. Only grid means only check statictetriminoes or not
export function isValidTetrimino(x, y, tetrimino, staticTetriminoes, onlyGrid = false) {
	for (let yi = y; yi < y + tetrimino.height; yi++)
		for (let xi = x; xi < x + tetrimino.width; xi++) {
			if (tetrimino.grid[(yi - y) * tetrimino.width + (xi - x)] === 0)
				continue;
			if (!onlyGrid) {
				if (yi < 0 || yi >= 20)
					return false;
				if (xi < 0 || xi >= 10)
					return false;
			}
			else {
				if (yi < 0 || yi >= 20)
					continue;
				if (xi < 0 || xi >= 10)
					continue;
			}
			if (staticTetriminoes[yi * 10 + xi] !== 0)
				return false;
		}
	return true;
}
