import store from '../store/index.js'
import {playerFall} from '../store/player.js'

let intervalId = undefined;

// stops player fall if possible
export function stopFall() {
    if (intervalId === undefined)
        return;
    clearInterval(intervalId);
    intervalId = undefined;
}

// starts or overrides player fall
export function startFall(ms) {
    if (intervalId !== undefined)
        stopFall()
    intervalId = setInterval(store.dispatch,  ms, playerFall());
}
// returns whether the player is falling
export function isFalling() {
    return intervalId !== undefined;
}
