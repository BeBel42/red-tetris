'use strict';

// function I use to return default color for each tetrimino
export default function getTetriminoColor(tetriminoId) {
	switch (tetriminoId) {
	  case 0:
		  return 2;
	  case 1:
		  return 3;
	  case 2:
		  return 4;
	  case 3:
		  return 5;
	  case 4:
		  return 6;
	  case 5:
		  return 7;
	  case 6:
		  return 8;
	}
}
