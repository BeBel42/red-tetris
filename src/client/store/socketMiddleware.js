'use strict';

import socket from '../socket/index.js'

const SEND_SOCKET = 'SEND_SOCKET'
const LISTEN_SOCKET = 'LISTEN_SOCKET'

// handles socket events
const socketMiddleware = store => next => action => {
	// handling socket events
	if (action.type === SEND_SOCKET) {
		socket.emit(action.event, action.data, action.callback);
	} else if (action.type === LISTEN_SOCKET) {
		socket.on(action.event, action.callback);
	}

	// Pass the action to the next middleware or the reducer
	const result = next(action);

	// Return the result of the next middleware or the reducer
	return result;
};


export default socketMiddleware;
