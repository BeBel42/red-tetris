import produce from 'immer'
import { stopFall } from '../utilities/fall.js';
import {getRotatedTetrimino} from '../utilities/tetriminoes.js'
import {defaultTetriminoes} from '../utilities/tetriminoes.js'
import {isValidTetrimino} from '../utilities/tetriminoes.js'
import {addStaticTetrimino} from './grid.js';
import {gameLost} from './game.js'
import fetchNewTetrimino from '../utilities/fetchNewTetrimino.js'
import getTetriminoColor from '../utilities/getTetriminoColor.js'
import {sendSocket} from './socket.js'
import socket from './index.js'

// rotates player by rotation levels if possible
export function rotatePlayer(rotation) {
	return (dispatch, getState) => {
		const player = getState().player;
		const grid = getState().grid;
		const rotatedTetrimino = getRotatedTetrimino(
			player.tetrimino, rotation
		);
		if (isValidTetrimino(
			player.x,
			player.y,
			rotatedTetrimino,
			grid.staticTetriminoes
		))
			dispatch(setPlayerTetrimino(rotatedTetrimino));
	}
}

// Moves player by x/y if possible
export function movePlayer(x, y) {
	return (dispatch, getState) => {
		const player = getState().player;
		const grid = getState().grid;
		if (!isValidTetrimino(
			player.x + x,
			player.y + y,
			player.tetrimino,
			grid.staticTetriminoes
		))
			return;
		dispatch(setPlayerPos(player.x + x, player.y + y));
	}
}

// gives to player anew tetrimino
export function newPlayerTetrimino() {
	return async (dispatch, getState) => {
		await fetchNewTetrimino();
		const tetriminoId = getState().game.stockTetrimino1.id;
		const color = getTetriminoColor(tetriminoId);
		const rotation = getState().game.stockTetrimino1.orientation;
		const newTetrimino = getRotatedTetrimino(
			defaultTetriminoes[tetriminoId], rotation
		);
		let yPos = 0;
		const xPos = Math.floor(5 - (newTetrimino.width / 2))
		if (!isValidTetrimino(
			xPos,
			yPos,
			newTetrimino,
			getState().grid.staticTetriminoes
		)) {
			dispatch(sendSocket('lost'));
			dispatch(gameLost());
			// avoid overlapping with highly-placed pieces
			while (!isValidTetrimino(
				xPos,
				yPos,
				newTetrimino,
				getState().grid.staticTetriminoes,
				true				// only static grid
			))
				yPos--;
		}
		// setting new player status
		dispatch(setPlayerPos(xPos, yPos));
		dispatch(setPlayerTetrimino(newTetrimino));
		dispatch(setPlayerColor(color));
	}
}

// Makes player fall / or attach his piece on the static grid
export function playerFall() {
	return (dispatch, getState) => {
		const player = getState().player;
		if (isValidTetrimino(
			player.x,
			player.y + 1,
			player.tetrimino,
			getState().grid.staticTetriminoes
		))
			return dispatch(movePlayer(0, 1));
		dispatch(addStaticTetrimino(
			player.tetrimino,
			0,
			player.x,
			player.y,
			player.color
		));
		dispatch(newPlayerTetrimino());
	}
}

// local setter action creators
const SET_PLAYER_POS = 'SET_PLAYER_POS';
export function setPlayerPos(x, y) {
	return {
		type: SET_PLAYER_POS,
		x,
		y
	}
}
// set next played tetrimino
const SET_PLAYER_TETRIMINO = 'SET_PLAYER_TETRIMINO';
function setPlayerTetrimino(tetrimino) {
	return {
		type: SET_PLAYER_TETRIMINO,
		tetrimino
	}
}
// tetrimino color
const SET_PLAYER_COLOR = 'SET_PLAYER_COLOR';
function setPlayerColor(color) {
	return {
		type: SET_PLAYER_COLOR,
		color
	}
}
// admin / player / etc.
const SET_PLAYER_TYPE = 'SET_PLAYER_TYPE';
export function setPlayerType(playerType) {
	return {
		type: SET_PLAYER_TYPE,
		playerType
	}
}

// player default state
const defaultState = {
	x: 0,
	y: 0,
	color: 1,
	tetrimino: defaultTetriminoes[0],
	type: '',
}
export function player(state=defaultState, action) { // reducer
	switch (action.type) {
	  case SET_PLAYER_POS:
		  return produce(state, (draft) => {
			  draft.x = action.x;
			  draft.y = action.y;
		  });
		  break;
	  case SET_PLAYER_TETRIMINO:
		  return produce(state, (draft) => {
			  draft.tetrimino = action.tetrimino;
		  });
		  break;
	  case SET_PLAYER_COLOR:
		  return produce(state, (draft) => {
			  draft.color = action.color;
		  });
		  break;
	  case SET_PLAYER_TYPE:
		  return produce(state, (draft) => {
			  draft.type = action.playerType;
		  });
		  break;
	  default:
		  return state;
	}
}
