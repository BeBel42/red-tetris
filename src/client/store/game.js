'use strict';

import produce from 'immer'
import { stopFall } from '../utilities/fall.js';
import socket from './index.js'
import {sendSocket} from './socket.js'

// game lost normally
const GAME_LOST = 'GAME_LOST';
export function gameLost(score) {
	return {
		type: GAME_LOST,
	}
}

// higher gravity == fast fall of player piece
const SET_GRAVITY = 'SET_GRAVITY';
export function setGravity(gravity) {
	return {
		type: SET_GRAVITY,
		gravity
	}
}

const SET_SCORE = 'SET_SCORE';
export function setScore(score) {
	return {
		type: SET_SCORE,
		score
	}
}

// player list for game room
const SET_PLAYER_LIST = 'SET_PLAYER_LIST';
export function setPlayerList(playerList) {
	return {
		type: SET_PLAYER_LIST,
		playerList
	}
}

// join game room
const SET_JOINED = 'SET_JOINED';
export function setJoined(joined) {
	return {
		type: SET_JOINED,
		joined
	}
}

// Game is launches
const SET_STARTED = 'SET_STARTED';
export function setStarted(started) {
	return {
		type: SET_STARTED,
		started
	}
}

// set preloaded tetriminoes
const SET_STOCK_TETRIMINOES = 'SET_STOCK_TETRIMINOES';
export function setStockTetriminoes(tetrimino1, tetrimino2, tetrimino3) {
	return {
		type: SET_STOCK_TETRIMINOES,
		tetrimino1,
		tetrimino2,
		tetrimino3,
	}
}

const SET_OTHER_PLAYERS_GRID = 'SET_OTHER_PLAYERS_GRID';
export function setOtherPlayerGrid(grid) {
	return {
		type: SET_OTHER_PLAYERS_GRID,
		grid,
	}
}

const SET_WINNER = 'SET_WINNER';
export function setWinner(winnerName) {
	return {
		type: SET_WINNER,
		winnerName,
	}
}

// pressing restart button after game ends
export function restartGameSession() {
	return (dispatch, getState) => {
		dispatch(sendSocket('restartGame'));
	}
}

// you got disconnected
const LOST_DUE_TO_RELOAD = 'LOST_DUE_TO_RELOAD ';
export function lostDueToReload() {
	return {
		type: LOST_DUE_TO_RELOAD ,
	}
}

const SET_SHOW_FULL_LINES = 'SET_SHOW_FULL_LINES';
export function setShowFullLines(showFullLines) {
	return {
		type: SET_SHOW_FULL_LINES,
		showFullLines,
	}
}

// player default state
export function game(
	//  forced to put it inline because of jest bug
	state={
		lostDueToReload: false,
		lost: false,
		score: 0,
		gravity: 3, // player will fall every 1/gravity seconds
		playerList: [],
		joined: false,
		started: false,
		stockTetrimino1: undefined,
		stockTetrimino2: undefined,
		stockTetrimino3: undefined,
		winner: '',
		showFullLines: false,
	},
	action
) { // reducer
	switch (action.type) {
	  case GAME_LOST:
		  return produce(state, (draft) => {
			  stopFall();
			  draft.lost = true;
		  });
		  break;
	  case SET_GRAVITY:
		  return produce(state, (draft) => {
			  draft.gravity = action.gravity;
		  });
		  break;
	  case SET_SCORE:
		  return produce(state, (draft) => {
			  draft.score = action.score;
		  });
		  break;
	  case SET_PLAYER_LIST:
		  return produce(state, (draft) => {
			  draft.playerList = action.playerList.map(i => ({
				  name: i,
				  board: new Array(10 * 20).fill(0),
			  }));
		  });
	  case SET_JOINED:
		  return produce(state, (draft) => {
			  draft.joined = action.joined;
		  });
		  break;
	  case SET_STARTED:
		  return produce(state, (draft) => {
			  draft.started = action.started;
		  });
		  break;
	  case SET_STOCK_TETRIMINOES:
		  return produce(state, (draft) => {
			  draft.stockTetrimino1 = action.tetrimino1;
			  draft.stockTetrimino2 = action.tetrimino2;
			  draft.stockTetrimino3 = action.tetrimino3;
		  });
		  break;
	  case SET_OTHER_PLAYERS_GRID:
		  return produce(state, (draft) => {
			  draft.playerList = action.grid;
		  });
		  break;
	  case SET_WINNER:
		  return produce(state, (draft) => {
			  draft.winner = action.winnerName;
		  });
		  break;
	  case LOST_DUE_TO_RELOAD:
		  return produce(state, (draft) => {
			  draft.lostDueToReload = true;
			  draft.lost = true;
		  });
		  break;
	  case SET_SHOW_FULL_LINES:
		  return produce(state, (draft) => {
			  draft.showFullLines = action.showFullLines;
		  });
		  break;
	  default:
		  return state;
	}
}
