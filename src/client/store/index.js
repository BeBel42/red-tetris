import { combineReducers } from 'redux';
import {grid} from './grid.js';
import {player} from './player.js';
import {game} from './game.js';
import {url} from './url.js';
import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger' // debug logging
import socketMiddleware from './socketMiddleware.js'
import thunk from 'redux-thunk'

// combining reducers
const rootReducer = combineReducers({
	grid,
	game,
	player,
	url,
});

// returning global store
export default createStore(
	rootReducer,
	applyMiddleware(
		thunk,
		socketMiddleware,
		// createLogger(),
	)
)
