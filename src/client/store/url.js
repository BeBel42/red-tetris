'use strict';

/*
  This represents data that can be retrieved from the url bar
  hostname/#roonname[playerName]
*/

import produce from 'immer'
import { stopFall } from '../utilities/fall.js';

const SET_ROOM_NAME = 'SET_ROOM_NAME';
export function setRoomName(roomName) {
	return {
		type: SET_ROOM_NAME,
		roomName
	}
}
const SET_PLAYER_NAME = 'SET_PLAYER_NAME';
export function setPlayerName(playerName) {
	return {
		type: SET_PLAYER_NAME,
		playerName
	}
}
// error while parsing url - invalid url
const SET_ERROR = 'SET_ERROR';
export function setError(error) {
	return {
		type: SET_ERROR,
		error
	}
}

// player default state
const defaultState = {
	roomName: undefined,
	playerName: undefined,
	error: undefined,
}
export function url(state=defaultState, action) { // reducer
	switch (action.type) {
	  case SET_ROOM_NAME:
		  return produce(state, (draft) => {
			  draft.roomName = action.roomName;
		  });
	  case SET_PLAYER_NAME:
		  return produce(state, (draft) => {
			  draft.playerName = action.playerName;
		  });
	  case SET_ERROR:
		  return produce(state, (draft) => {
			  draft.error = action.error;
		  });
	  default:
		  return state;
	}
}
