import produce from 'immer'
import {getRotatedTetrimino} from '../utilities/tetriminoes.js'
import {stopFall, startFall} from '../utilities/fall.js'
import {isValidTetrimino} from '../utilities/tetriminoes.js'
import {movePlayer} from './player.js'

// adds a tetrimino to the static tetrimino grid (aka the unmovable tetriminoes)
const ADD_STATIC_TETRIMINO = 'ADD_STATIC_TETRIMINO'
export function addStaticTetrimino(tetrimino, rotation, xPos, yPos, color) {
	return {
		type: ADD_STATIC_TETRIMINO,
		tetrimino: tetrimino,
		rotation: rotation,
		xPos: xPos,
		yPos: yPos,
		color: color,
	}
}
function addStaticTetriminoHanlder(draft, action) {
	const rotatedTetrimino = getRotatedTetrimino(action.tetrimino, action.rotation)
	for (let y = 0; y < action.tetrimino.height; y++)
		for (let x = 0; x < action.tetrimino.width; x++) {
			const index1D = (y + action.yPos) * 10 + x + action.xPos;
			if (rotatedTetrimino.grid[y * rotatedTetrimino.width + x] === 1
				&& index1D >= 0 && index1D < 10 * 20)
				draft.staticTetriminoes[index1D] = action.color;
		}
}

// Colors lines in white (lines that will get destroyed)
const SET_WHITE_LINES = 'SET_WHITE_LINES'
export function setWhiteLines(fullLinesArray) {
	return {
		type: SET_WHITE_LINES,
		fullLinesArray
	}
}
function setWhiteLinesHandler(draft, action) {
	for (const y of action.fullLinesArray)
		for (let x = 0; x < 10; x++)
			draft.staticTetriminoes[y * 10 + x] = 1; // setting white color
}

// Destroying lines colorid in white
const DESTROY_WHITE_LINES = 'DESTROY_WHITE_LINES'
export function destroyWhiteLines() {
	return {
		type: DESTROY_WHITE_LINES
	}
}
function destroyWhiteLinesandler(draft, action) {
	for (let y = 0; y < 20; y++)
	{
		if (draft.staticTetriminoes[y * 10] !== 1) continue;
		for (let x = 0; x < 10; x++) {
			draft.staticTetriminoes[y * 10 + x] = 0; // erasing case
			// moving pieces down
			for (let y2 = y - 1; y2 >= 0; y2--) {
				const tmp = draft.staticTetriminoes[(y2 + 1) * 10 + x];
				draft.staticTetriminoes[(y2 + 1) * 10 + x] = draft.staticTetriminoes[y2 * 10 + x];
				draft.staticTetriminoes[y2 * 10 + x] = tmp;
			}
		}
	}
}

// adds x rows og black lines for penalty
const ADD_BLACK_LINES = 'ADD_BLACK_LINES';
function addBlackLines(nOfLines) {
	return {
		type: ADD_BLACK_LINES,
		nOfLines,
	}
}
function addBlackLinesHandler(draft, action) {
	// moving pieces up
	for (let n = 0 ; n < action.nOfLines; n++)
		for (let y = 1; y < 20; y++)
			for (let x = 0; x < 10; x++)
				draft.staticTetriminoes[(y - 1) * 10 + x] = draft.staticTetriminoes[y * 10 + x]
	// setting new enpty lines to black
	for (let y = 19; y > 19 - action.nOfLines; y--)
		for (let x = 0; x < 10; x++)
			draft.staticTetriminoes[y * 10 + x] = 9;
}


// adds penalty lines + moves player up if necessary
export function addPenalty(nOfLines) {
	return (dispatch, getState) => {
		stopFall();
		dispatch(addBlackLines(nOfLines));
		const player = getState().player;
		let newY;
		for (newY = player.y; newY >= -player.tetrimino.height; newY--) {
			if (isValidTetrimino(
				player.x,
				newY,
				player.tetrimino,
				getState().grid.staticTetriminoes
			))
				break;
		}
		dispatch(movePlayer(0, newY - player.y));
		const time = 1000 / (getState().game.gravity);
		startFall(time);
	}
}

const defaultState = {
	staticTetriminoes: new Array(10 * 20).fill(0),
}
export function grid(state=defaultState, action) { // reducer
	switch (action.type) {
		case ADD_STATIC_TETRIMINO:
			return produce(state, (draft) => addStaticTetriminoHanlder(draft, action));
		case SET_WHITE_LINES:
			return produce(state, (draft) => setWhiteLinesHandler(draft, action));
		case DESTROY_WHITE_LINES:
			return produce(state, (draft) => destroyWhiteLinesandler(draft, action));
		case ADD_BLACK_LINES:
			return produce(state, (draft) => addBlackLinesHandler(draft, action));
		default:
			return state;
	}
}
