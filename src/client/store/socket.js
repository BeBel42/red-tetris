'use strict';

const SEND_SOCKET = 'SEND_SOCKET';
export function sendSocket(event, data, callback) {
	return {
		type: SEND_SOCKET,
		event,
		data,
		callback,
	}
}

const LISTEN_SOCKET = 'LISTEN_SOCKET';
export function listenSocket(event, callback) {
	return {
		type: LISTEN_SOCKET,
		event,
		callback,
	}
}
