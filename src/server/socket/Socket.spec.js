import { beforeEach, describe, expect, it} from "@jest/globals";
import {SocketMock} from "../../../test/utils/socket.mock.js";
import {IOMock} from "../../../test/utils/io.mock.js";
import Socket from "./Socket.js";
import {GameMock} from "../game/game.mock.js";
import Global from "../global.js";
import DBMock from "../db/db.mock.js";
import {Player} from "../player/player.js";

const sock = new SocketMock();
const io = new IOMock();

Global.db = new DBMock();

describe('socket', () => {
    it('should construct', () => {
        const socket = new Socket(io, sock);

        expect(socket.sock).toBeDefined();
        expect(socket.io).toBeDefined();
    });

    describe('functions', () => {
        let socket;
        beforeEach(() => {
            socket = new Socket(io, sock);
            socket.game = new GameMock();
        });

        describe('listen', () => {
            it('should listen', () => {
                expect(sock.on).toHaveBeenCalledTimes(10);
            });
        });

        describe('join', () => {
            it('should work', () => {
                const x = {
                    playerName: 'azea',
                    roomName: 'aaa'
                }
                socket.createGame = jest.fn(() => {});

                socket.join(x, () => {});
                expect(socket.name).toEqual(x.playerName);
                expect(socket.room).toEqual(x.roomName);
                expect(socket.createGame).toHaveBeenCalled();
            });
        });

        describe('createGame', () => {
            it('should work', () => {
                socket.createGame(() => {});
            });
        });

        describe('lineCleaned', () => {
            it('should work', () => {
                socket.lineCleaned(53);
            });
        });

        describe('start', () => {
            it('should work', () => {
                const grav = 4;
                const showFullLines = true;

                socket.start({
                    gravity: grav,
                    showFullLines: showFullLines
                });
                expect(socket.game.start).toHaveBeenCalledWith(grav, showFullLines, io);

            });
        });

        describe('updateInfos', () => {
            it('should work', () => {
                socket.updateInfos({
                    board: [],
                    score: 35
                });
            });
        });

        describe('nextPiece', () => {
            it('should work', () => {

                socket.nextPiece(1, () => {});

                expect(socket.game.getPiecesForPlayer).toHaveBeenCalled();

            });
        });

        describe('lost', () => {
            it('should work', () => {
                socket.lost();
                expect(socket.game.setPlayerLost).toHaveBeenCalled();
            });
        });

        describe('restartGame', () => {
            it('should work', () => {
                socket.restartGame();
            });
        });

        describe('mostRecentScores', () => {
            it('should work', async () => {
                const mockedFn = jest.fn();

                await socket.mostRecentScores(mockedFn);

                expect(mockedFn).toHaveBeenCalledWith([]);
            });
        });

        describe('top10Score', () => {
            it('should work', async () => {
                const mockedFn = jest.fn();

                await socket.top10Score(mockedFn);

                expect(mockedFn).toHaveBeenCalledWith([]);
            });
        });

        describe('onDisconnecting', () => {
            it('should work', () => {
                const player = new Player('Hey');
                socket.lost = jest.fn();
                socket.game.started = true;

                socket.onDisconnecting();
                expect(socket.lost).toHaveBeenCalled();
                expect(socket.game.getPlayer).toHaveBeenCalled();
                expect(socket.game.assignNewAdmin).toHaveBeenCalled();
            });
        });

    });
});
