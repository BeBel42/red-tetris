import {messagesInEnum, messagesOutEnum} from "../consts.js";
import {Game} from "../game/game.js";
import Global from "../global.js";

export default class Socket {

    sock;
    io;

    room = '';
    name = '';
    game;


    constructor(io, sock) {
        this.sock = sock;
        this.io = io;
        this.listen();
    }

    listen() {
        this.sock.on(messagesInEnum.join, (arg, callback) => this.join(arg, callback));
        this.sock.on(messagesInEnum.lineCleaned, (arg) => this.lineCleaned(arg));
        this.sock.on(messagesInEnum.start, (arg) => this.start(arg));
        this.sock.on(messagesInEnum.updateInfos, (arg) => this.updateInfos(arg));
        this.sock.on(messagesInEnum.nextPiece, (arg, callback) => this.nextPiece(arg, callback));
        this.sock.on(messagesInEnum.lost, () => this.lost());
        this.sock.on(messagesInEnum.restartGame, () => this.restartGame());
        this.sock.on(messagesInEnum.mostRecentScores, (_, callback) => this.mostRecentScores(callback));
        this.sock.on(messagesInEnum.top10Score, (_, callback) => this.top10Score(callback));
        this.sock.on("disconnecting", () => this.onDisconnecting());
    }

    join(arg, callback) {
        let {playerName, roomName} = arg;
        this.name = playerName;
        this.room = roomName;

        this.sock.join(this.room);
        if (Object.keys(Global.games).includes(this.room)) {
            this.game = Global.games[this.room];
            // if the user exists already
            if (this.game.players[this.name]) {
                if (this.game.players[this.name].connected) {
                    // already connected user
                    callback("spectator", this.game.playerList);
                    this.sock.emit(messagesOutEnum.updateBoards, this.game.boards)
                } else {
                    // refresh of a user
                    this.game.players[this.name].connected = true;
                    this.game.players[this.name].socketId = this.sock.id;
                    callback(this.game.admin === this.name ? "admin" : "player", this.game.playerList);
                    if (this.game.started) {
                        this.sock.emit(messagesOutEnum.lost);
                        if (this.game.playersAlive.length === 1) {
                            this.sock.emit(messagesOutEnum.winner, this.game.playersAlive[0]);
                        }
                    }
                }
            } else {
                if (this.game.started) {
                    // watching
                    callback("spectator", this.game.playerList);
                    this.sock.emit(messagesOutEnum.updateBoards, this.game.boards)
                } else {
                    // joining
                    this.game.addPlayer(this.name, this.sock.id);
                    callback("player", this.game.playerList);
                    this.sock.to(this.room).emit("playerListUpdate", {
                        players: this.game.playerList
                    });
                }
            }
        } else {
            this.createGame(callback);
        }
    }

    createGame(callback) {
        // Create game
        Global.games[this.room] = new Game(this.room, this.name);
        this.game = Global.games[this.room];
        // Then join as player
        this.game.addPlayer(this.name, this.sock.id);
        callback("admin", this.game.playerList);
    }

    lineCleaned(arg) {
        this.sock.to(this.room).emit("addLines", {
            fromPlayer: this.name,
            number: Number(arg) - 1
        });
    }

    start(arg) {
        let {gravity, showFullLines} = arg;
        this.game.start(gravity, showFullLines, this.io);
    }

    updateInfos(arg) {
        if (!this.game)
            return;
        let {board, score} = arg;
        if (board) {
            this.game.getPlayer(this.name).board = board;
            this.io.to(this.room).emit(messagesOutEnum.updateBoards, this.game.boards)
        }
        if (score)
            this.game.getPlayer(this.name).score = score;
    }

    nextPiece(arg, callback) {
        callback(this.game.getPiecesForPlayer(this.name));
    }

    lost() {
        // set lost
        this.game.setPlayerLost(this.name);
        Global.db.addScore(this.name, this.game.getPlayer(this.name).score);
        // end game or not?
        this.game.checkWinner(this.io);
    }

    restartGame() {
        delete Global.games[this.game.name];
        this.game = null;
        this.io.to(this.room).emit(messagesOutEnum.gameRestarted);
    }

    async mostRecentScores(callback) {
        callback(await Global.db.mostRecent);
    }

    async top10Score(callback) {
        callback(await Global.db.top10);
    }

    onDisconnecting() {
        // if is spectator or game doesn't exists
        if (!this.game || !this.game.getPlayer(this.name))
            return;
        if (this.game.started && this.game.getPlayer(this.name).alive) {
            this.lost();
        }
        this.game.getPlayer(this.name).connected = false;
        this.game.assignNewAdmin(this.io);
    }
}
