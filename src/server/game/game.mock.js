import {Player} from "../player/player.js";
import {Tetrimino} from "../tetrimino/tetrimino.js";
import {messagesOutEnum} from "../consts.js";
import Global from "../global.js";

export class GameMock {

	name = '';
	admin = '';
	players = {};
	started = false;
	pieces = [];
	gravity = 5;
	showFullLines = false;

	addPlayer = jest.fn();
	getPlayer = jest.fn(() => {
		return new Player('test');
	});
	setPlayerLost = jest.fn();
	getPiecesForPlayer = jest.fn();
	addTetriminos = jest.fn();
	start = jest.fn();
	checkWinner = jest.fn();
	assignNewAdmin = jest.fn();



	get playerList() {
		return [];
	}

	get boards() {
		return [];
	}

	get playersAlive() {
		return [];
	}

}
