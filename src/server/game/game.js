import {Player} from "../player/player.js";
import {Tetrimino} from "../tetrimino/tetrimino.js";
import {messagesOutEnum} from "../consts.js";
import Global from "../global.js";

export class Game {

	name = '';
	admin = '';
	players = {};
	started = false;
	pieces = [];
	gravity = 5;
	showFullLines = false;


	constructor(
		name,
		admin
	) {
		this.name = name;
		this.admin = admin;
	}

	addPlayer(name, socketId) {
		this.players[name] = new Player(name, socketId);
	}

	getPlayer(name) {
		return this.players[name];
	}

	setPlayerLost(name) {
		this.getPlayer(name).alive = false;
	}

	getPiecesForPlayer(name) {
		const index = this.getPlayer(name).piecesIndex;
		this.getPlayer(name).piecesIndex += 1;
		if (index > this.pieces.length - 4)
			this.addTetriminos();
		return this.pieces.slice(index, index + 3);
	}

	addTetriminos() {
		for (let i = 0; i < 3; i+=1) {
			this.pieces.push(new Tetrimino());
		}
	}

	start(gravity, showFullLines, io) {
		this.started = true;
		if (gravity)
			this.gravity = gravity;
		if (showFullLines)
			this.showFullLines = showFullLines;
		// tell everyone
		io.to(this.name).emit(messagesOutEnum.gameStarted, {
			gravity: this.gravity,
			showFullLines: this.showFullLines
		});
	}

	checkWinner(io) {
		if (this.playersAlive.length === 1) {
			io.to(this.name).emit(messagesOutEnum.winner, this.playersAlive[0]);
			Global.db.addScore(this.playersAlive[0], this.getPlayer(this.playersAlive[0]).score);
		}
	}

	assignNewAdmin(io) {
		if (Object.keys(this.players).length > 1) {
			const localAdmin = this.admin;
			for (let key of Object.keys(this.players)) {
				if (this.players[key].connected && this.players[key].name !== this.admin) {
					io.to(this.players[key].socketId).emit('admin', this.playerList);
					this.admin = this.players[key].name;
					return;
				}
			}
		}
	}

	get playerList() {
		return Object.keys(this.players);
	}

	get boards() {
		return Object.keys(this.players).map((player) => {
			return {
				name: player,
				board: this.players[player].board,
				// score: this.players[player].score,
			};
		});
	}

	get playersAlive() {
		return Object.keys(this.players).filter((player) => this.players[player].alive);
	}

}
