import {Player} from "../player/player.js";
import {emptyBoard, messagesOutEnum} from "../consts.js";
import {beforeEach, describe, expect, it} from "@jest/globals";
import {Game} from "./game.js";
import Global from "../global.js";
import DBMock from "../db/db.mock.js";
import {IOMock} from "../../../test/utils/io.mock.js";

Global.db = new DBMock();
describe('Game', () => {
	it('should have a working constructor', () => {
		const name = 'anme';
		const admin = 'andamkd';
		const game = new Game(name, admin);

		expect(game.name).toEqual(name);
		expect(game.admin).toEqual(admin);
	});

	describe('methods', () => {

		let game;
		const name = 'pierre';
		const admin = 'game';
		const player = new Player(admin);

		beforeEach(() => {
			game = new Game(name, admin);
			game.addPlayer(admin);
		});

		describe('addPlayer', () => {
			it('should add a player to the object', () => {
				const player = new Player(admin);

				game.addPlayer(admin);

				expect(game.players[admin]).toEqual(player);
			});
		});

		describe('getPlayer', () => {
			it('should add a player to the object', () => {
				expect(game.getPlayer(admin)).toEqual(player);
			});
		});

		describe('setPlayerLost', () => {
			it('should', () => {
				const player = new Player(admin);
				game.setPlayerLost(admin);

				expect(game.getPlayer(admin).alive).toEqual(false);
			});
		});

		describe('getPiecesForPlayer', () => {
			it('should', () => {

				game.addTetriminos = jest.fn(() => {});
				game.getPiecesForPlayer(admin);

				expect(game.addTetriminos).toHaveBeenCalledTimes(1);
			});
		});

		describe('addTetriminos', () => {
			it('should', () => {

				game.addTetriminos();

				expect(game.pieces.length).toEqual(3);
			});
		});

		describe('start', () => {
			it('should', () => {
				const io = new IOMock();
				const gravity = 123;
				const showFullLines = true;

				game.start(gravity, showFullLines, io);
				expect(game.started).toEqual(true)
				expect(game.gravity).toEqual(gravity)
				expect(game.showFullLines).toEqual(showFullLines)
				expect(io.toEmit).toHaveBeenCalledWith(
					messagesOutEnum.gameStarted,
					{
						gravity: gravity,
						showFullLines: showFullLines
					}
				);
			});
		});

		describe('checkWinner', () => {
			it('should', () => {

				const io = new IOMock();

				game.checkWinner(io);

				expect(io.to).toHaveBeenCalledWith(name);
				expect(io.toEmit).toHaveBeenCalledWith(messagesOutEnum.winner, admin);
			});
		});
	});


	describe('getters', () => {

		let game;
		const name = 'pierre';
		const admin = 'game';

		beforeEach(() => {
			game = new Game(name, admin);
			game.addPlayer(admin);
		});

		describe('playerList', () => {
			it('should', () => {

				expect(game.playerList).toEqual([admin]);
			});
		});

		describe('boards', () => {
			it('should return the boards', () => {
				const result = [{
					name: admin,
					board: emptyBoard
				}]
				expect(game.boards).toEqual(result);
			});
		});

		describe('playersAlive', () => {
			it('should', () => {

				expect(game.playersAlive).toEqual([admin]);
			});
		});
	});
});
