/* istanbul ignore file */

import * as http from "http";
import express from "express";
import {Server} from "socket.io";
import Socket from "./socket/Socket.js";




export class App {
	httpServer;
	port;
	io;

	constructor(port) {
		this.port = port;
		const app = this.getExpress();
		this.httpServer = new http.Server(app);
		this.io = new Server(this.httpServer);
		this.init();
	}

	getExpress() {
		const app = new express();
		app.use(express.static('public'));
		// removes file:// and app.js
		const root = import.meta.url.slice(7, -6) + 'public/';
		app.get('/bundle.js', (req, res) => {
			res.sendFile(root + 'bundle.js');
		});
		app.get('/*', (req, res) => {
			res.sendFile(root + 'index.html');
		});
		return app;
	}

	init() {
		this.io.on("connection", (socket) => {
			const x = new Socket(this.io, socket);
		});
	}

	start() {
		this.httpServer.listen(this.port, () => {
			console.log(`Server listening on http://localhost:${this.port}.`)
		});
	}
}
