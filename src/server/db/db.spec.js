import {MongoClient} from "mongodb";
import {beforeEach, describe, expect, it} from "@jest/globals";
import DB from "./db.js";

function mockMongoDB() {
    const originalModule = jest.requireActual('mongodb');

    //Mock the default export and named export 'foo'
    return {
        __esModule: true,
        ...originalModule,
        MongoClient: jest.fn().mockImplementation(() => {
            return {
                ...originalModule.MongoClient,
                connect: jest.fn(() => {}),
                db: jest.fn(() => {
                    return {
                        collection: jest.fn(() => {})
                    };
                })
            };
        })
    };
}

const clientConnect = jest.fn(() => {});
const clientDbCollectionInsertOne = jest.fn(() => {});
const clientDb = jest.fn(() => {
    return {
        collection: jest.fn(() => {
            return {
                insertOne: clientDbCollectionInsertOne,
                find: jest.fn(() => {
                    return {
                        sort: jest.fn(() => {
                            return {
                                limit: jest.fn(() => {
                                    return {
                                        toArray: jest.fn(() => [])
                                    };
                                })
                            };
                        })
                    };
                })
            };
        })
    };
});

// jest.mock('mongodb', () => mockMongoDB());
jest.mock('mongodb');
MongoClient.mockImplementation(() => {
    return {
        connect: clientConnect,
        db: clientDb
    };
})

describe('DB', () => {

    let db;
    beforeEach(async () => {
        db = new DB();
        await db.init();
    });

    describe('init', () => {
        it('should init all variables', async () => {

            expect(clientConnect).toHaveBeenCalled();
            expect(clientDb).toHaveBeenCalledWith('red-tetris');
        });
    });
    describe('addScore', () => {
        it('should init all variables', async () => {
            await db.addScore('a', 45);

            expect(clientDbCollectionInsertOne).toHaveBeenCalled();
        });
    });
    describe('top10', () => {
        it('should get array', async () => {
            const x = db.top10;

            expect(x).toEqual([]);
        });
    });
    describe('mostRecent', () => {
        it('should get array', async () => {
            const x = db.mostRecent;

            expect(x).toEqual([]);
        });
    });
});
