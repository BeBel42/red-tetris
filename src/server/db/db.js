import {MongoClient} from "mongodb";
/* istanbul ignore next */
import {config} from "dotenv";

config();

// Connection URL
const url = `mongodb+srv://${process.env.MONGOUSER}:${process.env.MONGOPASS}@${process.env.MONGODB}/?retryWrites=true&w=majority`;


export default class DB {

    collection;

    async init() {
        const client = new MongoClient(url);
        await client.connect();
        const db = client.db('red-tetris');
        this.collection = db.collection('scores');
    }

    addScore(username, score) {
        this.collection.insertOne({
            username: username,
            score: score,
            date: new Date()
        });
    }

    get top10() {
        return this.collection.find().sort({ score: -1 }).limit(10).toArray();
    }

    get mostRecent() {
        return this.collection.find().sort({ date: -1 }).limit(10).toArray();
    }

}
