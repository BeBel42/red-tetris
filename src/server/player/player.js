import {emptyBoard} from "../consts.js";

export class Player {
	piecesIndex = 0;
	score = 0;
	board = emptyBoard;
	name = '';
	alive = true;
	connected = true;
	socketId = 0;

	constructor(
		name,
		socketId
	) {
		this.name = name;
		this.socketId = socketId;
	}
}
