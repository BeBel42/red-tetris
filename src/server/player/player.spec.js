import {describe, expect, it} from "@jest/globals";
import {Player} from "./player.js";


describe("Player", () => {
	it('should set the name on construct', () => {
		// Act
		const expectedName = "name111";
		const player = new Player(expectedName);
		// Assert
		expect(player.name).toEqual(expectedName);
	});
});
