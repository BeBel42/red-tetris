import {Tetrimino} from "./tetrimino.js";
import {expect} from "@jest/globals";

describe("Tetriminos", () => {
	it("should init with random variables", () => {
		// Act
		const tet = new Tetrimino();
		// Assert
		expect(tet.id).toBeGreaterThan(-1);
		expect(tet.id).toBeLessThan(7);

		expect(tet.orientation).toBeGreaterThan(-1);
		expect(tet.orientation).toBeLessThan(4);

		expect(tet.color).toBeGreaterThan(-1);
		expect(tet.color).toBeLessThan(6);
	});
});
