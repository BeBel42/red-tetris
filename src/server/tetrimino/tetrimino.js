export class Tetrimino {
	id;
	orientation;
	color;

	constructor(
	) {
		this.id = Math.floor(Math.random() * 7);
		this.orientation = Math.floor(Math.random() * 4);
		this.color = Math.ceil(Math.random() * 5);
	}
}
