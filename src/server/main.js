import {App} from './app.js';
import DB from "./db/db.js";
import Global from "./global.js";

const port = 3004;

Global.db = new DB();
await Global.db.init();
new App(port).start();
