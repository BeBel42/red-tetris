/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/configuration
 */

export default {
  // All imported modules in your tests should be mocked automatically
  // automock: false,

  // Stop running tests after `n` failures
  // bail: 0,

  // The directory where Jest should store its cached dependency information
  // cacheDirectory: "/tmp/jest_rs",

  // Automatically clear mock calls, instances, contexts and results before every test
  clearMocks: true,

  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,

  // An array of glob patterns indicating a set of files for which coverage information should be collected
  // collectCoverageFrom: undefined,

  // The directory where Jest should output its coverage files
  coverageDirectory: "coverage",

  // An array of regexp pattern strings used to skip coverage collection
  // coveragePathIgnorePatterns: [
  //   "/node_modules/"
  // ],

  // Indicates which provider should be used to instrument code for coverage
  coverageProvider: "v8",

  collectCoverageFrom: [
    "<rootDir>/src/**/*.js",
    "<rootDir>/src/**/*.jsx",
    "!<rootDir>/src/**/*.mock.js",
    "!<rootDir>/src/**/app.js",
    "!<rootDir>/src/**/global.js",
    "!<rootDir>/src/client/index.jsx",
    "!<rootDir>/src/server/main.js"
  ],

  coverageThreshold: {
    global: {
      branches: 70,
      functions: 70,
      lines: 70,
      statements: 70,
    },
  },

  testMatch: [
    "<rootDir>/test/**/*.test.js",
    "<rootDir>/src/**/*.spec.js"
  ],

  testPathIgnorePatterns : [
    "<rootDir>/test/utils/",
    "<rootDir>/node-modules/"
  ],

  moduleNameMapper: {
    '^.+\\.(css|less)$': '<rootDir>/CSStub.js'
  }
};
